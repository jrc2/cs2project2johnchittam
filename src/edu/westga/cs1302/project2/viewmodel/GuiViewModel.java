package edu.westga.cs1302.project2.viewmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.net.URL;
import java.time.LocalDate;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;

import edu.westga.cs1302.project2.model.Validator;
import edu.westga.cs1302.project2.model.utils.Utils;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;
import edu.westga.cs1302.project2.model.TypeThenDateComparator;
import edu.westga.cs1302.project2.controller.CalendarBookController;
import edu.westga.cs1302.project2.datatier.TextFileLoader;
import edu.westga.cs1302.project2.datatier.TextFileWriter;
import edu.westga.cs1302.project2.datatier.TextLoader;
import edu.westga.cs1302.project2.datatier.TextUrlLoader;
import edu.westga.cs1302.project2.model.CalendarBook;

import edu.westga.cs1302.project2.resources.UI;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The controller manages all the events.
 * 
 * @author CS1302
 * @version 1.0
 */
public class GuiViewModel {

	private CalendarBook calendarBook; // stores all the events loaded or created

	private StringProperty descriptionProperty;
	private ObjectProperty<LocalDate> dateProperty;
	private StringProperty nYearProperty;
	private StringProperty sigLevelProperty;
	private StringProperty errorMessageProperty;
	private ObservableList<Event> eventList; // stores the events to be shown in the listview (could be all the events
												// or search
												// result)

	/**
	 * Creates an instance of the GuiViewModel class.
	 * 
	 * @precondition: none
	 * @postcondition: All the needed fields are initialized.
	 */
	public GuiViewModel() {
		this.calendarBook = new CalendarBook();

		this.descriptionProperty = new SimpleStringProperty("");

		this.dateProperty = new SimpleObjectProperty<LocalDate>();
		this.nYearProperty = new SimpleStringProperty("");
		this.sigLevelProperty = new SimpleStringProperty("");

		this.errorMessageProperty = new SimpleStringProperty("");
		this.eventList = FXCollections.observableArrayList();

	}

	/**
	 * Get the CalendarBook object.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the CalendarBook object.
	 */
	public CalendarBook getCalendarBook() {
		return this.calendarBook;
	}

	/**
	 * Get the description property.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the string property
	 */
	public StringProperty descriptionProperty() {
		return this.descriptionProperty;
	}

	/**
	 * Get the date property.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the date property
	 */
	public ObjectProperty<LocalDate> dateProperty() {
		return this.dateProperty;
	}

	/**
	 * Get the nYear property.
	 *
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the nYear property
	 */
	public StringProperty nYearProperty() {
		return this.nYearProperty;
	}

	/**
	 * Get the sigLevel property.
	 *
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the sigLevel property
	 */
	public StringProperty sigLevelProperty() {
		return this.sigLevelProperty;
	}

	/**
	 * Get the errorMessage property.
	 *
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the errorMessage property
	 */
	public StringProperty errorMessageProperty() {
		return this.errorMessageProperty;
	}

	/**
	 * Get the eventListProperty.
	 *
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the eventListProperty
	 */

	public ObservableList<Event> eventListProperty() {
		return this.eventList;
	}

	/**
	 * add a new event object.
	 * 
	 * @param type the type of the Event to be added
	 * 
	 * @precondition: none
	 * @postcondition: this.eventListProperty().size() ==
	 *                 this.eventListProperty().size()@pre +1
	 * 
	 * @return true if the event is added successfully, false otherwise.
	 */
	public boolean addEvent(String type) {
		Validator validator = new Validator();
		String nYearString = this.nYearProperty.get();
		String sigLevelString = this.sigLevelProperty.get();
		int nyear = 0;
		int sigLevel = 0;
		
		try {
			String description = validator.validateDescription(this.descriptionProperty.get());
			LocalDate date = validator.validateDate(this.dateProperty.get());
			type = validator.validateType(type);
			
			if (this.eventValidator(type) == null) {
				return false;
			} else if (this.eventValidator(type).equals(EventType.NYEAR)) {
				nyear = Integer.parseInt(validator.validateTextField(nYearString).trim());
			} else if (this.eventValidator(type).equals(EventType.ONETIME)) {
				sigLevel = Integer.parseInt(validator.validateTextField(sigLevelString).trim());
			}
						
			if (this.calendarBook.add(Utils.createEvent(type, date, description, nyear, sigLevel))) {
				return this.eventList.setAll(this.calendarBook.getEvents());
			} else {
				this.errorMessageProperty.set("the event was not added");
			}
			
		} catch (Exception e) {
			this.errorMessageProperty = new SimpleStringProperty(e.getMessage());
			return false;
		}

		return false;
	}
	
	private EventType eventValidator(String type) {
		EventType eventType = EventType.parseType(type);
		Validator validator = new Validator();
		String nYearString = this.nYearProperty.get();
		String sigLevelString = this.sigLevelProperty.get();
		
		if (eventType.equals(EventType.NYEAR)) {
			String validatedNYear = validator.validateTextField(nYearString);
			if (validatedNYear == null) {
				this.errorMessageProperty.set(UI.ExceptionMessages.NULL_FIELD_N);
				return null;
			}
			
			return EventType.NYEAR;
			
		} else if (eventType.equals(EventType.ONETIME)) {
			String validatedSigLevel = validator.validateTextField(sigLevelString);
			if (validatedSigLevel == null) {
				this.errorMessageProperty.set(UI.ExceptionMessages.NULL_SIG_LEVEL_FIELD);
				return null;
			}
			
			return EventType.ONETIME;
			
		}
		
		return eventType;
	}
	
	/**
	 * Load random events to the event list.
	 * 
	 * @precondition none
	 * @postcondition random events added to event list
	 */
	public void loadRandom() {
		CalendarBookController calendarController = new CalendarBookController();
		calendarController.generateRandomEvents();
		this.setEventList(calendarController.getCalendarBook().getEvents());
	}
	
	/**
	 * Gets the earliest event by type.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 */
	public void getEarliestEventByType() {
		CalendarBookController calendarController = new CalendarBookController();
		this.eventList.clear();
		this.setEventList(calendarController.getEarliestEventByType(this.calendarBook.getEvents()));
	}

	/**
	 * Load the events from a file.
	 * 
	 * @param file the file from which events are to be loaded
	 * 
	 * @precondition: none
	 * @postcondition: the calendarBook is unchanged if the file is empty;otherwise,
	 *                 all the non-duplicate events of the file are added to the
	 *                 calendarBook and the events are shown in the ListView.
	 * 
	 * @throws IOException if file not found
	 */

	public void loadFile(File file) throws IOException {
		
		TextFileLoader fileLoader = new TextFileLoader(file);
		
		this.setEventList(fileLoader);

	}

	private void setEventList(TextLoader fileLoader) throws IOException {
		this.setEventList(fileLoader.loadData());
	}

	private void setEventList(List<Event> events) {
		for (Event currEvent : events) {
			boolean added = this.calendarBook.add(currEvent);
			if (!added) {
				this.errorMessageProperty.set("Duplicate events are not added!");
			}
			
		}
		
		this.eventList.clear();
		this.eventList.addAll(this.calendarBook.getEvents());
		
	}

	/**
	 * Loads the events from a URL resource.
	 * 
	 * @param url the url to the resource where the data is stored
	 * 
	 * @precondition: none
	 * @postcondition: the calendarBook is unchanged if the resource at the
	 *                 specified url is empty; otherwise, all the non-duplicate
	 *                 events specified in the URL resource are added to the
	 *                 calendarBook and the events are shown in the ListView.
	 * @throws IOException if IOException thrown
	 */
	public void loadUrl(URL url) throws IOException {
		if (url.openStream().toString().isEmpty()) {
			return;
		}
		
		TextUrlLoader urlLoader = new TextUrlLoader(url);
		
		this.setEventList(urlLoader);
	}

	/**
	 * reload the calendarBook events into the ListView
	 * 
	 * @precondition: none
	 * @postcondition: the calendarBook events are loaded into the ListView
	 * 
	 */
	public void reloadEvents() {

		if (this.calendarBook.size() != 0) {
			this.eventList.clear();
			this.eventList.setAll(this.calendarBook.getEvents());
		} else {
			this.errorMessageProperty.set(UI.ExceptionMessages.NO_EVENT_ERROR_MESSAGE);
		}
	}

	/**
	 * Saves the events in the ListView into a file represented by
	 * filename
	 * 
	 * @param file name of the file that the events are saved to.
	 * 
	 * @precondition: none
	 * @postcondition: file exists if there are events in the ListView
	 * 
	 * @throws IOException if IOException is thrown
	 * 
	 */
	public void saveFile(File file) throws IOException {
		TextFileWriter fileWriter = new TextFileWriter(file);
		
		fileWriter.save(this.eventList);
	}

	/**
	 * Saves the events in the ListView in natural ordering in four
	 * groups (Daily, Monthly, NYear, Onetime) in the format of the following into a
	 * file represented by filename.
	 * 
	 * TYPE: size = xx yyyy-mm-dd, description yyyy-mm-dd, description
	 * 
	 * @param file name of the file that the events are saved to.
	 * 
	 * @precondition: none
	 * @postcondition: file exists if there are events in the ListView
	 * 
	 * @throws IOException if IOException thrown
	 * 
	 */
	public void saveFile2(File file) throws IOException {
		TextFileWriter fileWriter = new TextFileWriter(file);
		
		fileWriter.save2(this.eventList);
	}
	
	/**
	 * Save file 3.
	 *
	 * @param file name of the file that the events are saved to.
	 * 
	 * @precondition: none
	 * @postcondition: file exists if there are events in the ListView
	 * 
	 * @throws IOException if IOException thrown
	 */
	public void saveFile3(File file) throws IOException {
		TextFileWriter fileWriter = new TextFileWriter(file);
		
		fileWriter.save3(this.eventList);
	} 

	/**
	 * The method shows in the ListView all the events that occur on the given date
	 * or contain the given description or occur on the given date and contain the
	 * given description (if both fields are filled).
	 * 
	 * 
	 * The user may pick a date in the datePicker, or enter a description, or pick a
	 * date and enter a description in the GUI.
	 * 
	 * The method looks for events in the calendarBook that either occur on the date
	 * specified by the date that the user picked (if any) or have description that
	 * contains the phrase specified in the description (if any) or occur on the
	 * date specified by the user and have description that contains the phrase
	 * specified in the description. If the user doesn't pick a date, or enter a
	 * description, the method should set the errorMessageProperty to prompt the
	 * user "Please choose a date or enter a description, or both!"
	 * 
	 * if no events found satisfy the conditions, it sets the errorMessageProperty
	 * to indicate that. otherwise, it clears the eventList and add the found events
	 * to the eventList.
	 * 
	 * Create private helper methods when needed.
	 * 
	 * aDate stores the LocalDate in the date picker. description stores the
	 * description that user specified.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 */
	public void search() {

		LocalDate aDate = this.dateProperty.get();
		String description = this.descriptionProperty.get();
		List<Event> matches = new ArrayList<Event>();

		if (aDate == null && description.isEmpty()) {
			this.errorMessageProperty.set("Please choose a date or enter a description, or both!");
		} else if (aDate != null) {
			matches.addAll(this.searchDateAndDescription(aDate, description));
		} else {
			matches.addAll(this.searchDescription(description));
		}
		
		if (matches.size() == 0) {
			this.errorMessageProperty.set("no matches");
		} else {
			this.eventList.clear();
			this.eventList.addAll(matches);
		}
	}
	
	private List<Event> searchDateAndDescription(LocalDate date, String description) {
		List<Event> events = new ArrayList<Event>();
		
		for (Event curr : this.calendarBook.getEvents()) {
			if (curr.getDate().equals(date) && curr.getDescription().toUpperCase().contains(description.toUpperCase())) {
				events.add(curr);
			}
		}
		
		return events;
	}
	
	private List<Event> searchDescription(String description) {
		List<Event> events = new ArrayList<Event>();
		
		for (Event curr : this.calendarBook.getEvents()) {
			if (curr.getDescription().toUpperCase().contains(description.toUpperCase())) {
				events.add(curr);
			}
		}
		
		return events;
	}

	/**
	 * Gets the events in the calendarBook and sorts them in their
	 * natural ordering and displays them in the ListView.
	 * 
	 * To make the events show up in the ListView, need to clear the eventList and
	 * add sorted events to the eventList.
	 * 
	 * @precondition: none
	 * @postcondition: the eventList contains a list of events sorted in the
	 *                 specified order.
	 * 
	 * 
	 */
	public void sortByDate() {
		List<Event> calendarBook = new ArrayList<Event>(this.calendarBook.getEvents());
		Collections.sort(calendarBook);
		this.eventList.clear();
		this.eventList.addAll(calendarBook);
	}

	/**
	 * Gets the events in the calendarBook and sorts them by type in
	 * descending order followed by date in descending order and displays them in the
	 * ListView.
	 * 
	 * @precondition: none
	 * @postcondition: the eventList contains a list of events sorted in the
	 *                 specified order.
	 * 
	 */
	public void sortByTypeDate() {
		List<Event> calendarBook = new ArrayList<Event>(this.calendarBook.getEvents());
		Collections.sort(calendarBook, new TypeThenDateComparator());
		this.eventList.clear();
		this.eventList.addAll(calendarBook);
	}

	private boolean isEmpty(List<Event> events) {
		if (events.size() == 0) {
			this.errorMessageProperty.set(UI.ExceptionMessages.NO_EVENT_ERROR_MESSAGE);
			return true;
		}
		return false;
	}

	/**
	 * Clear the calendarBook and the eventList.
	 * 
	 * @precondition: none
	 * @postcondition: calendarBook and eventList are empty.
	 */
	public void clearCalendarBookandListView() {
		this.calendarBook.clear();
		this.eventList.clear();
	}

}
