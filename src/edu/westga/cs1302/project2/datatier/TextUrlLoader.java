package edu.westga.cs1302.project2.datatier;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;
import edu.westga.cs1302.project2.model.utils.Utils;

/**
 * The Class TextUrlLoader.
 * 
 * @author john chittam
 * 
 */
public class TextUrlLoader implements TextLoader {
	private URL textResource;
	
	/**
	 * Instantiates a new text url loader.
	 * 
	 * @precondition URL exists
	 * @postcondition none
	 *
	 * @param textResource the text resource
	 */
	public TextUrlLoader(URL textResource) {
		this.textResource = textResource;
	}

	@Override
	public List<Event> loadData() throws IOException {
		Scanner scan = null;
		ArrayList<String> fileLines = new ArrayList<String>();
		
		try {
			scan = new Scanner(this.textResource.openStream());
			while (scan.hasNextLine()) {
				fileLines.add(scan.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} finally {
			scan.close();
		}
		
		return this.parseFileLines(fileLines);
	}
	
	private ArrayList<Event> parseFileLines(ArrayList<String> fileLines) {
		ArrayList<Event> events = new ArrayList<Event>();

		for (String currLine : fileLines) {
			try {
				String[] lineArray = currLine.split("\\|");
				ArrayList<String> line = new ArrayList<String>(Arrays.asList(lineArray));
				EventType type = EventType.parseType(line.get(0).toString());
				line.remove(0);
				int sigLevel = 0;
				if (type.equals(EventType.ONETIME)) {
					sigLevel = Integer.parseInt(line.get(0));
					line.remove(0);
				}
				int nyear = 0;
				if (type.equals(EventType.NYEAR)) {
					nyear = Integer.parseInt(line.get(0));
					line.remove(0);
				}
				LocalDate date = LocalDate.parse(line.get(0));
				line.remove(0);
				String description = line.get(0);
				new Utils();
				events.add(Utils.createEvent(type.toString(), date, description, nyear, sigLevel));
			} catch (Exception e) {
				System.err.println("Error in reading the file: " + e.getMessage() + " " + currLine);
			}
		}
		
		return events;
	}
}
