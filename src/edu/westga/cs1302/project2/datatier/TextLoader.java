package edu.westga.cs1302.project2.datatier;

import java.io.IOException;
import java.util.List;

import edu.westga.cs1302.project2.model.Event;

/**
 * The Interface TextLoader.
 * 
 * @author john chittam
 * 
 */
public interface TextLoader {
	
	/**
	 * Load data.
	 *
	 * @return the list of data
	 * @throws IOException if file not found
	 */
	List<Event> loadData() throws IOException;
}
