package edu.westga.cs1302.project2.datatier;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.westga.cs1302.project2.model.Daily;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.Monthly;
import edu.westga.cs1302.project2.model.NYear;
import edu.westga.cs1302.project2.resources.UI;

/**
 * The Class TextFileWriter.
 * 
 * @author john chittam
 * 
 */
public class TextFileWriter {
	private File file;
	
	/**
	 * Instantiates a new text file writer.
	 * 
	 * @precondition file!=null
	 * @postcondition none
	 *
	 * @param file the file
	 */
	public TextFileWriter(File file) {
		if (file == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_FILE);
		}
		
		this.file = file;
	}
	
	/**
	 * Saves the given events to a text file.
	 * 
	 * @precondition events!=null AND !events.isEmpty()
	 * @postcondition file has been written
	 *
	 * @param events the events to write
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void save(List<Event> events) throws IOException {
		if (events == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_EVENTLIST);
		}
		if (events.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_EVENTLIST);
		}
		
		List<Event> sortedEvents = new ArrayList<Event>(events);
		Collections.sort(sortedEvents);
		
		try (PrintWriter writer = new PrintWriter(this.file)) {
			for (Event curr : sortedEvents) {
				writer.println(curr.getDate().toString() + UI.TextFormatting.COMMA_SPACE + curr.getClass().getSimpleName().toUpperCase()
					+ UI.TextFormatting.COMMA_SPACE + curr.getDescription());
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	/**
	 * Saves the given events to a text file sorted by type
	 * 
	 * @precondition events!=null AND !events.isEmpty()
	 * @postcondition file has been written
	 *
	 * @param events the events to sort and write
	 */
	public void save2(List<Event> events) {
		if (events == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_EVENTLIST);
		}
		if (events.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_EVENTLIST);
		}
		
		List<Event> sortedEvents = new ArrayList<Event>(events);
		Collections.sort(sortedEvents);
		
		try (PrintWriter writer = new PrintWriter(this.file)) {
			writer.println(this.formatEventsByType(sortedEvents));
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	private String formatEventsByType(List<Event> sortedEvents) {
		ArrayList<Event> dailyEvents = new ArrayList<Event>();
		ArrayList<Event> monthlyEvents = new ArrayList<Event>();
		ArrayList<Event> onetimeEvents = new ArrayList<Event>();
		ArrayList<Event> nyearEvents = new ArrayList<Event>();
		
		for (Event currEvent : sortedEvents) {
			if (currEvent instanceof Daily) {
				dailyEvents.add(currEvent);
			} else if (currEvent instanceof Monthly) {
				monthlyEvents.add(currEvent);
			} else if (currEvent instanceof NYear) {
				nyearEvents.add(currEvent);
			} else {
				onetimeEvents.add(currEvent);
			}
		}
		
		return "DAILY: size = " + dailyEvents.size() + System.lineSeparator() + this.save2Formatter(dailyEvents)
			+ "MONTHLY: size = " + monthlyEvents.size() + System.lineSeparator() + this.save2Formatter(monthlyEvents)
			+ "NYEAR: size = " + nyearEvents.size() + System.lineSeparator() + this.save2Formatter(nyearEvents)
			+ "ONETIME: size = " + onetimeEvents.size() + System.lineSeparator() + this.save2Formatter(onetimeEvents);
		
	}
	
	private String save2Formatter(List<Event> events) {
		String output = "";
		
		for (Event currEvent : events) {
			output += "      " + currEvent.getDate() + ", " + currEvent.getDescription() + System.lineSeparator();
		}
		
		return output;
	}
	
	/**
	 * Saves the given events to a text file sorted by type
	 * 
	 * @precondition events!=null AND !events.isEmpty()
	 * @postcondition file has been written
	 *
	 * @param events the events to sort and write
	 */
	public void save3(List<Event> events) {
		if (events == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_EVENTLIST);
		}
		if (events.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_EVENTLIST);
		}
		
		List<Event> sortedEvents = new ArrayList<Event>(events);
		Collections.sort(sortedEvents);
		
		List<LocalDate> eventDates = new ArrayList<LocalDate>();
		for (Event curr : sortedEvents) {
			if (!eventDates.contains(curr.getDate())) {
				eventDates.add(curr.getDate());
			}
		}
		
		try (PrintWriter writer = new PrintWriter(this.file)) {
			writer.println(this.save3EventsByDate(eventDates, sortedEvents));
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	private String save3EventsByDate(List<LocalDate> eventDates, List<Event> sortedEvents) {
		String output = "";
		
		for (int i = 0; i < eventDates.size(); i++) {
			LocalDate thisDay = eventDates.get(i);
			List<Event> thisDayEvents = new ArrayList<Event>();
			for (Event curr : sortedEvents) {
				if (curr.getDate().equals(thisDay)) {
					thisDayEvents.add(curr);
				}
			}
			output += "Events occured on the same day" + System.lineSeparator();
			for (int j = 1; j <= thisDayEvents.size(); j++) {
				output += "event " + j + ": " + thisDayEvents.get(j - 1).toString() + System.lineSeparator();
			}
			output += "==>Grouped by event type:" + System.lineSeparator() + this.formatEventsByType(thisDayEvents) + System.lineSeparator();
		}
		
		return output;
	}
}
