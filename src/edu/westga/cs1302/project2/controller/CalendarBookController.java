package edu.westga.cs1302.project2.controller;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import edu.westga.cs1302.project2.model.CalendarBook;
import edu.westga.cs1302.project2.model.Daily;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.Monthly;
import edu.westga.cs1302.project2.model.NYear;
import edu.westga.cs1302.project2.model.utils.Utils;

/**
 * CalendarBookController generates random events and gets events grouped by
 * types.
 * 
 * @author CS1302
 * @version 1.0
 *
 */
public class CalendarBookController {

	private static String[] descriptions = { "dental appointment", "car maintenance", "dance lessons",
		"drawing lessongs", "birthday party", "grocery shopping" };

	private CalendarBook calendarBook;

	/**
	 * Instantiates a new calendar book controller.
	 *
	 * @precondition: none
	 * @postcondition: getCalendarBook().size()==0
	 */
	public CalendarBookController() {
		this.calendarBook = new CalendarBook();
	}

	

	/**
	 * Generates random events and adds them into the caldenarBook.
	 * 
	 * Randomly generates a number between 20 and 30. Creates that number of Onetime,
	 * Monthly, Daily and NYear events
	 * 
	 * For each event, the date should be a random number within 0-364 days after
	 * today, the description should be any random description in the
	 * descriptions array, the nyear should be a random number b/w 1 to the length of
	 * the descriptions array, the sigLevel should be a random number b/w 1- 10.
	 * 
	 * @precondition: none
	 * @postcondition: calendarBook has 20-30 randomly generated events of each type
	 * 
	 */
	public void generateRandomEvents() {
		Random rand = new Random();
		int numOfEvents = rand.nextInt((30 - 20) + 1) + 20;
		Event[] onetimeEvents = this.generateRandomEvents(numOfEvents, "Onetime");
		Event[] monthlyEvents = this.generateRandomEvents(numOfEvents, "Monthly");
		Event[] dailyEvents = this.generateRandomEvents(numOfEvents, "Daily");
		Event[] nyearEvents = this.generateRandomEvents(numOfEvents, "NYear");
		
		this.addAllToCalendarBook(onetimeEvents);
		this.addAllToCalendarBook(monthlyEvents);
		this.addAllToCalendarBook(dailyEvents);
		this.addAllToCalendarBook(nyearEvents);
		
	}
	
	private Event[] generateRandomEvents(int numOfEvents, String type) {
		Event[] events = new Event[numOfEvents];
		Random rand = new Random();
		for (int i = 0; i < numOfEvents; i++) {
			int daysPastToday = rand.nextInt(365);
			LocalDate date = LocalDate.now().plusDays(daysPastToday);
			int descriptionCount = rand.nextInt(CalendarBookController.descriptions.length);
			String description = CalendarBookController.descriptions[descriptionCount];
			int nyear = rand.nextInt((CalendarBookController.descriptions.length - 1) + 1) + 1;
			int sigLevel = rand.nextInt((10 - 1) + 1) + 1;
			events[i] = Utils.createEvent(type, date, description, nyear, sigLevel);
		}
		
		return events;
	}
	
	private void addAllToCalendarBook(Event[] events) {
		for (Event curr : events) {
			this.calendarBook.add(curr);
		}
	}

	/**
	 * Gets the calendar book.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 * 
	 * @return the calendar book
	 */
	public CalendarBook getCalendarBook() {
		return this.calendarBook;
	}

	/**
	 * Gets the earliest event by type.
	 * 
	 * @precondition: none
	 * @postcondition: none
	 *
	 * @return the earliest event by type
	 * @param events the events to sort
	 */
	public List<Event> getEarliestEventByType(List<Event> events) {
		List<Event> earliestEvents = new ArrayList<Event>();
		ArrayList<Event> onetimeEvents = new ArrayList<Event>();
		ArrayList<Event> dailyEvents = new ArrayList<Event>();
		ArrayList<Event> monthlyEvents = new ArrayList<Event>();
		ArrayList<Event> nyearEvents = new ArrayList<Event>();
		for (Event curr : events) {
			if (curr instanceof NYear) {
				nyearEvents.add(curr);
			} else if (curr instanceof Daily) {
				dailyEvents.add(curr);
			} else if (curr instanceof Monthly) {
				monthlyEvents.add(curr);
			} else {
				onetimeEvents.add(curr);
			}
		}
		
		earliestEvents.add(this.getEarliest(onetimeEvents));
		earliestEvents.add(this.getEarliest(dailyEvents));
		earliestEvents.add(this.getEarliest(monthlyEvents));
		earliestEvents.add(this.getEarliest(nyearEvents));
		
		return earliestEvents;
	}
	
	private Event getEarliest(List<Event> events) {
		Collections.sort(events);
		return events.get(0);
	}

}
