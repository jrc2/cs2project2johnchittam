package edu.westga.cs1302.project2.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

import edu.westga.cs1302.project2.resources.UI;

/**
 * The abstract Event class
 * 
 * @author john chittam
 *
 */
public abstract class Event implements Comparable<Event> {
	
	private LocalDate date;
	private String description;
	
	/**
	 * Event constructor
	 * 
	 * @precondition date!=null AND !date.isBefore(LocalDate.now()) AND description!=null AND !description.isEmpty()
	 * @postcondition getDate()==date AND getYear()==date.getYear() AND getMonthValue()==date.getMonthValue AND getDescription()==description
	 * 
	 * @param date the date of the event
	 * @param description the event's description
	 */
	protected Event(LocalDate date, String description) {
		if (date == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_DATE);
		}
		if (date.isBefore(LocalDate.now())) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EARLIER_THAN_TODAY);
		}
		if (description == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_DESCRIPTION);
		}
		if (description.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_DESCRIPTION);
		}
		
		this.date = date;
		this.description = description;
	}
	
	/**
	 * Constructs an event with today's date
	 * 
	 * @precondition description!=null AND !description.isEmpty()
	 * @postcondition getDate()==LocalDate.now() AND getDescription()==description
	 * 
	 * @param description the event's description
	 */
	protected Event(String description) {
		this(LocalDate.now(), description);
	}
	
	/**
	 * Gets the date.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the date
	 */
	public LocalDate getDate() {
		return this.date;
	}
	
	/**
	 * Gets the year.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year
	 */
	public int getYear() {
		return this.date.getYear();
	}
	
	/**
	 * Gets the month value.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the month value
	 */
	public int getMonthValue() {
		return this.date.getMonthValue();
	}
	
	/**
	 * Gets the day of the month of the event.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the day of the month of the event
	 */
	public int getDayOfMonth() {
		return this.date.getDayOfMonth();
	}
	
	/**
	 * Gets the description.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}
	
	@Override
	public String toString() {		
		return this.date.getMonthValue() + UI.TextFormatting.SLASH + this.date.getDayOfMonth() + UI.TextFormatting.SLASH
			+ this.date.getYear() + UI.TextFormatting.COMMA_SPACE + this.getClass().getSimpleName() 
			+ UI.TextFormatting.COMMA_SPACE + this.description;
	}
	
	@Override
	public boolean equals(Object otherObject) {
		Event otherEvent = (Event) otherObject;
		
		if (this == otherObject) {
			return true;
		} else if (otherObject == null) {
			return false;
		} else if (this.getClass() != otherObject.getClass()) {
			return false;
		} else if (this.date == otherEvent.getDate()) {
			return this.description.equals(otherEvent.getDescription());  
		} else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(this.date, this.description);
	}
	
	/**
	 * Occurs on abstract method.
	 *
	 * @param date the date
	 * @return controlled on implementation
	 */
	public abstract boolean occursOn(LocalDate date);
	
	@Override
	public int compareTo(Event otherEvent) {
		int thisEventDate = Integer.parseInt("" + this.getYear() + this.getMonthValue() + this.getDayOfMonth());
		int otherEventDate = Integer.parseInt("" + otherEvent.getYear() + otherEvent.getMonthValue() + otherEvent.getDayOfMonth());
		
		if (thisEventDate > otherEventDate) {
			return 1;
		} else if (thisEventDate < otherEventDate) {
			return -1;
		} else {
			return 0;
		}
	}
	
}
