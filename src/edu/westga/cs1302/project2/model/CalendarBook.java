package edu.westga.cs1302.project2.model;

import java.util.ArrayList;
import java.util.List;

import edu.westga.cs1302.project2.resources.UI;

/**
 * The CalendarBook class
 * 
 * @author john chittam
 *
 */
public class CalendarBook {
	private List<Event> events;
	
	/**
	 * Instantiates a new calendar book.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public CalendarBook() {
		this.events = new ArrayList<Event>();
	}
	
	/**
	 * Instantiates a new calendar book with the given list of events
	 * 
	 * @precondition events!=null AND !events.isEmpty()
	 * @postcondition this.events.getSize()==events.size()
	 * 
	 * @param events the events to add to the new calendar book
	 */
	public CalendarBook(List<Event> events) {
		this.events = events;
	}
	
	/**
	 * Gets the size of the calendar book
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the size of the calendar book
	 */
	public int size() {
		return this.events.size();
	}
	
	/**
	 * Gets the events in the calendar book
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of events in the calendar book
	 */
	public List<Event> getEvents() {
		return this.events;
	}
	
	/**
	 * Adds the specified event to the events list
	 * 
	 * @precondition event!=null
	 * @postcondition none
	 * 
	 * @param event the event to add
	 * @return true if added, false if it already exists or otherwise was not added
	 */
	public boolean add(Event event) {
		if (event == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_EVENT);
		}
		
		for (Event curr : this.events) {
			if (curr.toString().trim().equals(event.toString().trim())) {
				return false;
			}
		}
		
		return this.events.add(event);
	}
	
	/**
	 * Checks if the events list contains an event
	 * 
	 * @precondition event!=null
	 * @postcondition none
	 * 
	 * @param event the event to check for
	 * @return true if this.events contains the event, false if not
	 */
	public boolean contains(Event event) {
		if (event == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_EVENT);
		}
		
		return this.events.contains(event);
	}
	
	/**
	 * Removes all events in the Calendar book
	 * 
	 * @precondition none
	 * @postcondition events.size()==0
	 */
	public void clear() {
		this.events.clear();
	}
}
