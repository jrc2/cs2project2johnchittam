package edu.westga.cs1302.project2.model.utils;

import java.time.LocalDate;

import edu.westga.cs1302.project2.model.Daily;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.EventType;
import edu.westga.cs1302.project2.model.Monthly;
import edu.westga.cs1302.project2.model.NYear;
import edu.westga.cs1302.project2.model.Onetime;

/**
 * The Utils class
 * 
 * @author john chittam
 *
 */
public class Utils {
	
	/**
	 * Creates an event.
	 * 
	 * @precondition EventType.parseType(type) is valid type AND date>=LocalDate.now() AND description!=null
	 * 				 !description.isEmpty() AND nyear>=1 AND 1<=sigLevel<=10
	 * @postcondition Event instanceof type AND getDate()==date AND getDescription()==description
	 * 				  AND getN()--nyear (if type==NYEAR) AND getSigLevel()==sigLevel (if type==ONETIME)
	 *
	 * @param type the type of event
	 * @param date the date of the event
	 * @param description the event's description
	 * @param nyear the nyear
	 * @param sigLevel the significance level of the event
	 * @return the new event
	 */
	public static Event createEvent(String type, LocalDate date, String description, int nyear, int sigLevel) {
		EventType eventType = null;
		
		try {
			eventType = EventType.parseType(type);
		} catch (IllegalArgumentException e) {
			System.out.println("Invalid type...assuming one time event with sigLevel 1");
		}
		
		if (eventType == null) {
			return new Onetime(date, description, 1);
		} else if (eventType.equals(EventType.DAILY)) {
			return new Daily(date, description);
		} else if (eventType.equals(EventType.MONTHLY)) {
			return new Monthly(date, description);
		} else if (eventType.equals(EventType.ONETIME)) {
			return new Onetime(date, description, sigLevel);
		} else if (eventType.equals(EventType.NYEAR)) {
			return new NYear(date, description, nyear);
		} else {
			return new Onetime(date, description, 1);
		}
	}
}
