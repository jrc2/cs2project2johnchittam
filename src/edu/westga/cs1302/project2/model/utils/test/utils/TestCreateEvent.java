package edu.westga.cs1302.project2.model.utils.test.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Daily;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.Monthly;
import edu.westga.cs1302.project2.model.Onetime;
import edu.westga.cs1302.project2.model.utils.Utils;

class TestCreateEvent {

	@Test
	void testCreateDaily() {
		Event daily = Utils.createEvent("daily", LocalDate.parse("2019-12-25"), "christmas", 1, 2);
		
		assertAll(
			()-> assertTrue(daily instanceof Daily),
			()-> assertEquals(LocalDate.parse("2019-12-25"), daily.getDate()),
			()-> assertEquals("christmas", daily.getDescription())
		);
	}
	
	@Test
	void testCreateInvalidType() {
		Event other = Utils.createEvent("other", LocalDate.parse("2020-01-23"), "other", 1, 3);
		
		assertAll(
			()-> assertTrue(other instanceof Onetime),
			()-> assertEquals(LocalDate.parse("2020-01-23"), other.getDate()),
			()-> assertEquals("other", other.getDescription()),
			()-> assertEquals(1, ((Onetime) other).getSigLevel())
		);
	}
	
	@Test
	void testCreateMonthly() {
		Event monthly = Utils.createEvent("  monthly  ", LocalDate.parse("2020-01-23"), "test monthly", 1, 7);
		
		assertAll(
			()-> assertTrue(monthly instanceof Monthly),
			()-> assertEquals(LocalDate.parse("2020-01-23"), monthly.getDate()),
			()-> assertEquals("test monthly", monthly.getDescription())
		);
	}
	
	@Test
	void testCreateOnetime() {
		Event onetime = Utils.createEvent("  oNeTiMe  ", LocalDate.parse("2020-01-23"), "test onetime", 1, 7);
		
		assertAll(
			()-> assertTrue(onetime instanceof Onetime),
			()-> assertEquals(LocalDate.parse("2020-01-23"), onetime.getDate()),
			()-> assertEquals("test onetime", onetime.getDescription()),
			()-> assertEquals(7, ((Onetime) onetime).getSigLevel())
		);
	}

}
