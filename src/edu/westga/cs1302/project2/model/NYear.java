package edu.westga.cs1302.project2.model;

import java.time.LocalDate;
import java.util.Objects;

import edu.westga.cs1302.project2.resources.UI;

/**
 * The Class NYear.
 * 
 * @author john chittam
 * 
 */
public class NYear extends Event {
	private int n;
	
	/**
	 * Instantiates a new NYear Event with set date
	 * 
	 * @precondition date!=null AND !date.isBefore(LocalDate.now()) AND description!=null AND !description.isEmpty()
	 * 				 AND n>=1
	 * @postcondition getDate()==date AND getYear()==date.getYear() AND getMonthValue()==date.getMonthValue 
	 * 				  AND getDayOfMonth()==date.getDayOfMonth AND getDescription()==description AND getN()==n
	 *
	 * @param date the date
	 * @param description the description
	 * @param n the number of years between events
	 */
	public NYear(LocalDate date, String description, int n) {
		super(date, description);
		
		if (n < 1) {
			throw new IllegalArgumentException(UI.ExceptionMessages.N_OUT_OF_RANGE);
		}
		
		this.n = n;
	}
	
	/**
	 * Instantiates a new NYear Event for today's date.
	 * 
	 * @precondition description!=null AND !description.isEmpty() AND n>=1
	 * @postcondition getDate()==LocalDate.now() AND getYear()==LocalDate.now().getYear AND getMonthValue()==LocalDate.now().getMonthValue
	 * 				  AND getDayOfMonth()==LocalDate.now().getDayOfMonth AND getDescription()==description AND getN()==n
	 *
	 * @param description the description
	 * @param n the number of years between events
	 */
	public NYear(String description, int n) {
		this(LocalDate.now(), description, n);
	}
	
	/**
	 * Gets n
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return n
	 */
	public int getN() {
		return this.n;
	}
	
	@Override
	public String toString() {
		return super.toString() + ", Every " + this.n + " years";
	}

	/**
	 * Appends the event's class hashCode to the end of the super class hash code
	 */
	@Override
	public int hashCode() {
		int superClassHash = super.hashCode();
		int eventHash = Objects.hashCode(this.getClass().toString());
		return Integer.parseInt(superClassHash + "" + eventHash);
	}
	
	/**
	 * This is here because of the hashCode method above. It doesn't change anything.
	 */
	@Override
	public boolean equals(Object otherObject) {
		return super.equals(otherObject);
	}

	@Override
	public boolean occursOn(LocalDate date) {
		if (this.getDate().equals(date) || (this.getMonthValue() == date.getMonthValue() && this.getDayOfMonth()
			== date.getDayOfMonth() && (date.getYear() - this.getYear()) % this.n == 0) && this.getYear() < date.getYear()) {
			return true;
		}
		
		return false;
	}
	
}
