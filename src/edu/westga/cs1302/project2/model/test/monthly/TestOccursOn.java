package edu.westga.cs1302.project2.model.test.monthly;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Monthly;

class TestOccursOn {

	@Test
	void testDayOf() {
		Monthly event = new Monthly("my daily event");
		assertTrue(event.occursOn(LocalDate.now()));
	}
	
	@Test
	void testOneDayBefore() {
		Monthly event = new Monthly(LocalDate.parse("2019-12-25"), "monthly event");
		assertFalse(event.occursOn(LocalDate.parse("2019-12-24")));
	}
	
	@Test
	void testOneDayAfter() {
		Monthly event = new Monthly(LocalDate.parse("2019-12-25"), "monthly event");
		assertFalse(event.occursOn(LocalDate.parse("2019-12-26")));
	}
	
	@Test
	void testAfterButNotSameDayOfMonth() {
		Monthly event = new Monthly(LocalDate.parse("2019-12-25"), "monthly event");
		assertFalse(event.occursOn(LocalDate.parse("2020-01-15")));
	}
	
	@Test
	void testAfterSameDayOfMonth() {
		Monthly event = new Monthly(LocalDate.parse("2019-12-25"), "monthly event");
		assertTrue(event.occursOn(LocalDate.parse("2020-03-25")));
	}

}
