package edu.westga.cs1302.project2.model.test.typethendatecomparator;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Daily;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.Monthly;
import edu.westga.cs1302.project2.model.NYear;
import edu.westga.cs1302.project2.model.Onetime;
import edu.westga.cs1302.project2.model.TypeThenDateComparator;

class TestCompare {

	@Test
	void test() {
		List<Event> events = new ArrayList<Event>();
		Event onetime1 = new Onetime(LocalDate.parse("2020-02-20"), "onetime1 one", 5);
		Event onetime2 = new Onetime(LocalDate.parse("2021-02-20"), "onetime2 one", 5);
		Event monthly1 = new Monthly(LocalDate.parse("2020-02-20"), "monthly1 one");
		Event monthly2 = new Monthly(LocalDate.parse("2021-02-20"), "monthly2 one");
		Event daily1 = new Daily(LocalDate.parse("2020-02-20"), "daily1 one");
		Event daily2 = new Daily(LocalDate.parse("2021-02-20"), "daily2 one");
		Event nyear1 = new NYear(LocalDate.parse("2020-02-20"), "nyear1 one", 2);
		Event nyear2 = new NYear(LocalDate.parse("2021-02-20"), "nyear2 one", 2);
		events.add(onetime1);
		events.add(onetime2);
		events.add(monthly1);
		events.add(monthly2);
		events.add(daily1);
		events.add(daily2);
		events.add(nyear1);
		events.add(nyear2);
		
		Collections.sort(events, new TypeThenDateComparator());
		
		assertAll(
			()-> assertEquals(onetime2, events.get(0)),
			()-> assertEquals(onetime1, events.get(1)),
			()-> assertEquals(nyear2, events.get(2)),
			()-> assertEquals(nyear1, events.get(3)),
			()-> assertEquals(monthly2, events.get(4)),
			()-> assertEquals(monthly1, events.get(5)),
			()-> assertEquals(daily2, events.get(6)),
			()-> assertEquals(daily1, events.get(7)),
			()-> assertEquals(8, events.size())
		);
	}

}
