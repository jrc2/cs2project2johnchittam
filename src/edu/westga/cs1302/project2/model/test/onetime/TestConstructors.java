package edu.westga.cs1302.project2.model.test.onetime;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Onetime;

class TestConstructors {

	@Test
	void testEverythingValid() {
		Onetime event = new Onetime(LocalDate.parse("2019-12-25"), "christmas party", 4);
		
		assertAll(
			()-> assertEquals(LocalDate.parse("2019-12-25"), event.getDate()),
			()-> assertEquals(2019, event.getYear()),
			()-> assertEquals(12, event.getMonthValue()),
			()-> assertEquals(25, event.getDayOfMonth()),
			()-> assertEquals("christmas party", event.getDescription()),
			()-> assertEquals(4, event.getSigLevel())
		);
	}
	
	@Test
	void testLowerBoundSigLevel() {
		assertThrows(IllegalArgumentException.class, ()-> new Onetime(LocalDate.now(), "desc", 0));
	}
	
	@Test
	void testUpperBoundSigLevel() {
		assertThrows(IllegalArgumentException.class, ()-> new Onetime(LocalDate.now(), "desc", 11));
	}
	
	@Test
	void testWayTooHighSigLevel() {
		assertThrows(IllegalArgumentException.class, ()-> new Onetime(LocalDate.now(), "desc", Integer.MAX_VALUE));
	}
	
	@Test
	void testWayTooLowSigLevel() {
		assertThrows(IllegalArgumentException.class, ()-> new Onetime(LocalDate.now(), "desc", Integer.MIN_VALUE));
	}
	
	@Test
	void testOneParamConstructor() {
		Onetime event = new Onetime("party", 5);
		assertEquals(LocalDate.now(), event.getDate());
	}

}
