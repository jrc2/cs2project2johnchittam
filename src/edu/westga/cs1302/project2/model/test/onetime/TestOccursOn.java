package edu.westga.cs1302.project2.model.test.onetime;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Onetime;

class TestOccursOn {

	@Test
	void testSameDay() {
		Onetime event = new Onetime("my event", 2);
		assertTrue(event.occursOn(LocalDate.now()));
	}
	
	@Test
	void testOneDayAfter() {
		Onetime event = new Onetime(LocalDate.parse("2019-12-25"), "christmas party", 4);
		assertFalse(event.occursOn(LocalDate.parse("2019-12-26")));
	}
	
	@Test
	void testFarAfter() {
		Onetime event = new Onetime(LocalDate.parse("2019-12-25"), "christmas party", 4);
		assertFalse(event.occursOn(LocalDate.parse("2029-02-16")));
	}
	
	@Test
	void testOneDayBefore() {
		Onetime event = new Onetime(LocalDate.parse("2019-12-25"), "christmas party", 4);
		assertFalse(event.occursOn(LocalDate.parse("2019-12-24")));
	}
	
	@Test
	void testFarBefore() {
		Onetime event = new Onetime(LocalDate.parse("2019-12-25"), "christmas party", 4);
		assertFalse(event.occursOn(LocalDate.parse("2009-02-16")));
	}
	
	@Test
	void testSameDayDifferentMonth() {
		Onetime event = new Onetime(LocalDate.parse("2019-12-25"), "christmas party", 4);
		assertFalse(event.occursOn(LocalDate.parse("2020-03-25")));
	}

}
