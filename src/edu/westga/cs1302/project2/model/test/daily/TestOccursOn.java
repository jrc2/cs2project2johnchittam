package edu.westga.cs1302.project2.model.test.daily;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Daily;

class TestOccursOn {

	@Test
	void testOccursToday() {
		Daily daily = new Daily("my daily event");
		
		assertTrue(daily.occursOn(LocalDate.now()));
	}
	
	@Test
	void testOneDayAfter() {
		Daily daily = new Daily(LocalDate.parse("2019-12-25"), "christmas party");
		assertTrue(daily.occursOn(LocalDate.parse("2019-12-26")));
	}
	
	@Test
	void testFarAfter() {
		Daily daily = new Daily(LocalDate.parse("2019-12-25"), "christmas party");
		assertTrue(daily.occursOn(LocalDate.parse("2022-01-01")));
	}
	
	@Test
	void testFarBefore() {
		Daily daily = new Daily(LocalDate.parse("2019-12-25"), "christmas party");
		assertFalse(daily.occursOn(LocalDate.parse("2009-01-01")));
	}
	
	@Test
	void testOneDayBefore() {
		Daily daily = new Daily(LocalDate.parse("2019-12-25"), "christmas party");
		assertFalse(daily.occursOn(LocalDate.parse("2019-12-24")));
	}
}
