package edu.westga.cs1302.project2.model.test.daily;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Daily;

class TestOneParamConstructor {

	@Test
	void testEverythingValid() {
		Daily daily = new Daily("christmas party");
		
		assertAll(
			()-> assertEquals(LocalDate.now(), daily.getDate()),
			()-> assertEquals(LocalDate.now().getYear(), daily.getYear()),
			()-> assertEquals(LocalDate.now().getMonthValue(), daily.getMonthValue()),
			()-> assertEquals("christmas party", daily.getDescription())
		);
	}
	
	@Test
	void testNullDescription() {
		assertThrows(IllegalArgumentException.class, ()-> new Daily(null));
	}
	
	@Test
	void testEmptyDescription() {
		assertThrows(IllegalArgumentException.class, ()-> new Daily(""));
	}

}
