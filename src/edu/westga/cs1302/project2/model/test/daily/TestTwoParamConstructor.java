package edu.westga.cs1302.project2.model.test.daily;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.Daily;

class TestTwoParamConstructor {

	@Test
	void testEverythingValid() {
		Daily daily = new Daily(LocalDate.parse("2019-12-25"), "christmas party");
		
		assertAll(
			()-> assertEquals(LocalDate.parse("2019-12-25"), daily.getDate()),
			()-> assertEquals(2019, daily.getYear()),
			()-> assertEquals(12, daily.getMonthValue()),
			()-> assertEquals(25, daily.getDayOfMonth()),
			()-> assertEquals("christmas party", daily.getDescription())
		);
	}
	
	@Test
	void testNullDate() {
		assertThrows(IllegalArgumentException.class, ()-> new Daily(null, "christmas party"));
	}
	
	@Test
	void testDateBeforeToday() {
		assertThrows(IllegalArgumentException.class, ()-> new Daily(LocalDate.parse("2010-12-25"), "christmas party"));
	}
	
	@Test
	void testNullDescription() {
		assertThrows(IllegalArgumentException.class, ()-> new Daily(LocalDate.parse("2029-12-25"), null));
	}
	
	@Test
	void testEmptyDescription() {
		assertThrows(IllegalArgumentException.class, ()-> new Daily(LocalDate.parse("2029-12-25"), ""));
	}

}
