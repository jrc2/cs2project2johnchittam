package edu.westga.cs1302.project2.model.test.nyear;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.project2.model.NYear;

class TestOccursOn {

	@Test
	void testValid() {
		NYear event = new NYear(LocalDate.parse("2020-01-22"), "party", 3);
		
		assertAll(
			()-> assertTrue(event.occursOn(LocalDate.parse("2020-01-22"))), //same day
			()-> assertFalse(event.occursOn(LocalDate.parse("2021-01-22"))), //less than n years later
			()-> assertTrue(event.occursOn(LocalDate.parse("2023-01-22"))), //n years later
			()-> assertFalse(event.occursOn(LocalDate.parse("2017-01-22"))) //n years before
		);
	}

}
