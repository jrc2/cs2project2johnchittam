package edu.westga.cs1302.project2.model;

import java.time.LocalDate;
import java.util.Objects;

import edu.westga.cs1302.project2.resources.UI;

/**
 * The Class Onetime; for onetime events
 * 
 * @author john chittam
 *
 */
public class Onetime extends Event {
	private int sigLevel;
	
	/**
	 * Instantiates a new Onetime Event with set date.
	 * 
	 * @precondition date!=null AND !date.isBefore(LocalDate.now()) AND description!=null AND !description.isEmpty()
	 * 				 AND 1 <= sigLevel <= 10
	 * @postcondition getDate()==date AND getYear()==date.getYear() AND getMonthValue()==date.getMonthValue 
	 * 				  AND getDayOfMonth()==date.getDayOfMonth AND getDescription()==description AND getSigLevel()==sigLevel
	 *
	 * @param date the event date
	 * @param description the event's description
	 * @param sigLevel the significance level
	 */
	public Onetime(LocalDate date, String description, int sigLevel) {
		super(date, description);
		
		if (sigLevel < 1 || sigLevel > 10) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SIGLEVEL_NOT_IN_RANGE);
		}
		
		this.sigLevel = sigLevel;
	}
	
	/**
	 * Instantiates a new Onetime Event with today's date.
	 * 
	 * @precondition description!=null AND !description.isEmpty() AND 1 <= sigLevel <= 10
	 * @postcondition getDate()==LocalDate.now() AND getYear()==LocalDate.now().getYear AND getMonthValue()==LocalDate.now().getMonthValue
	 * 				  AND getDayOfMonth()==LocalDate.now().getDayOfMonth AND getDescription()==description AND getSigLevel()==sigLevel
	 *
	 * @param description the event's description
	 * @param sigLevel the significance level
	 */
	public Onetime(String description, int sigLevel) {
		this(LocalDate.now(), description, sigLevel);
	}
	
	/**
	 * Gets the event's significance level.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the event's significance level
	 */
	public int getSigLevel() {
		return this.sigLevel;
	}
	
	/**
	 * Returns true if the given date matches this.getDate(), false otherwise
	 */
	@Override
	public boolean occursOn(LocalDate date) {
		if (date.equals(this.getDate())) {
			return true;
		}
		
		return false;
	}
	
	
	@Override
	public String toString() {
		return super.toString() + ", SignificanceLevel = " + this.sigLevel;
	}
	
	/**
	 * Appends the event's class hashCode to the end of the super class hash code
	 */
	@Override
	public int hashCode() {
		int superClassHash = super.hashCode();
		int eventHash = Objects.hashCode(this.getClass().toString());
		return Integer.parseInt(superClassHash + "" + eventHash);
	}
	
	/**
	 * This is here because of the hashCode method above. It doesn't change anything.
	 */
	@Override
	public boolean equals(Object otherObject) {
		return super.equals(otherObject);
	}
}
