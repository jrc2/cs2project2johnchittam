package edu.westga.cs1302.project2.model;

import edu.westga.cs1302.project2.resources.UI;

/**
 * The Enum EventType
 * 
 * @author john chittam
 *
 */
public enum EventType {
	ONETIME, DAILY, MONTHLY, NYEAR;
	
	/**
	 * Parses a string and returns an event type
	 * 
	 * @precondition type!=null AND !type.isEmpty() AND EventType.values() contains type
	 * 
	 * @param type the string representation of the type to return
	 * @return the type based on the string representation
	 */
	public static EventType parseType(String type) {
		if (type == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_TYPE);
		}
		if (type.trim().isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.EMPTY_TYPE);
		}
		
		type = type.trim().toUpperCase();
				
		try {
			EventType eventType = EventType.valueOf(type);
			return eventType;
			
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_TYPE + " "  + type);
		}
		
	}
}
