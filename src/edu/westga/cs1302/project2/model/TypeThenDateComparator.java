package edu.westga.cs1302.project2.model;

import java.util.Comparator;

/**
 * Sorts events by type in descending order then by date in descending order
 * 
 * @author john chittam
 * 
 */
public class TypeThenDateComparator implements Comparator<Event> {

	@Override
	public int compare(Event e1, Event e2) {
		if (e1.getClass().getSimpleName().compareTo(e2.getClass().getSimpleName()) < 0) {
			return 1;
		} else if (e1.getClass().getSimpleName().compareTo(e2.getClass().getSimpleName()) > 0) {
			return -1;
		} else {
			return e1.compareTo(e2) * -1;
		}
	}

}
