package edu.westga.cs1302.project2.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * The Daily class which extends Event
 * 
 * @author john chittam
 *
 */
public class Daily extends Event {
	
	/**
	 * Instantiates a new Daily Event with set date
	 * 
	 * @precondition date!=null AND !date.isBefore(LocalDate.now()) AND description!=null AND !description.isEmpty()
	 * @postcondition getDate()==date AND getYear()==date.getYear() AND getMonthValue()==date.getMonthValue 
	 * 				  AND getDayOfMonth()==date.getDayOfMonth AND getDescription()==description
	 *
	 * @param date the date
	 * @param description the description
	 */
	public Daily(LocalDate date, String description) {
		super(date, description);
	}
	
	/**
	 * Instantiates a new Daily Event for today's date.
	 * 
	 * @precondition description!=null AND !description.isEmpty()
	 * @postcondition getDate()==LocalDate.now() AND getYear()==LocalDate.now().getYear AND getMonthValue()==LocalDate.now().getMonthValue
	 * 				  AND getDayOfMonth()==LocalDate.now().getDayOfMonth AND getDescription()==description
	 *
	 * @param description the description
	 */
	public Daily(String description) {
		super(description);
	}
	
	/**
	 * Returns true if the given date is on or after the event date
	 */
	@Override
	public boolean occursOn(LocalDate date) {
		if (this.getDate().compareTo(date) <= 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Appends the Daily class hashCode to the end of the super class hash code
	 */
	@Override
	public int hashCode() {
		int superClassHash = super.hashCode();
		int dailyHash = Objects.hashCode(this.getClass().toString());
		return Integer.parseInt(superClassHash + "" + dailyHash);
	}
	
	/**
	 * This is here because of the hashCode method above. It doesn't change anything.
	 */
	@Override
	public boolean equals(Object otherObject) {
		return super.equals(otherObject);
	}
}
