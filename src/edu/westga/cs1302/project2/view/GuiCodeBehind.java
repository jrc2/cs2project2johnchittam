package edu.westga.cs1302.project2.view;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import edu.westga.cs1302.project2.resources.UI;
import edu.westga.cs1302.project2.viewmodel.GuiViewModel;
import edu.westga.cs1302.project2.model.Event;
import edu.westga.cs1302.project2.model.Daily;
import edu.westga.cs1302.project2.model.Monthly;
import edu.westga.cs1302.project2.model.NYear;
import edu.westga.cs1302.project2.model.Onetime;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleGroup;

/**
 * GuiCodeBehind defines the "controller" for Gui.fxml.
 * 
 * @author CS1302
 * @version 1.0
 */
public class GuiCodeBehind {

	private GuiViewModel viewModel;

	@FXML
	private TextField descriptionTextField;

	@FXML
	private Button addButton;

	@FXML
	private RadioButton dailyRadioButton;

	@FXML
	private ToggleGroup group;

	@FXML
	private RadioButton monthlyRadiobutton;

	@FXML
	private RadioButton onetimeRadioButton;

	@FXML
	private RadioButton nyearRadioButton;

	@FXML
	private Button clearButton;

	@FXML
	private ListView<Event> eventListView;

	@FXML
	private Button searchButton;

	@FXML
	private Button reloadButton;

	@FXML
	private Button clearListButton;

	@FXML
	private DatePicker eventDate;

	@FXML
	private TextField nyearTextField;

	@FXML
	private TextField sigLevelTextField;

	@FXML
	private MenuItem loadFileMenuItem;

	@FXML
	private MenuItem loadWebMenuItem;

	@FXML
	private MenuItem loadRandomMenuItem;

	@FXML
	private MenuItem saveMenuItem;
	@FXML
	private MenuItem saveMenuItem2;
	@FXML
	private MenuItem saveMenuItem3;

	@FXML
	private MenuItem exitMenuItem;

	@FXML
	private MenuItem sortbyDateMenuItem;

	@FXML
	private MenuItem sortbyTypeDateMenuItem;

	@FXML
	private MenuItem earliestDateByTypeMenuItem;

	@FXML
	private Label errorMessageLabel;

	/**
	 * Create a GuiController object.
	 */
	public GuiCodeBehind() {

		this.viewModel = new GuiViewModel();

	}

	/**
	 * Initializes the GUI components, binding them to the view model properties,
	 * setListener for ListView and error message.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	@FXML
	public void initialize() {

		this.bindComponentsToViewModel();
		this.setupListenerForListView();
		this.setupListenerForErrorMessage();

	}

	private void bindComponentsToViewModel() {
		this.descriptionTextField.textProperty().bindBidirectional(this.viewModel.descriptionProperty());

		this.eventDate.valueProperty().bindBidirectional(this.viewModel.dateProperty());

		this.nyearTextField.textProperty().bindBidirectional(this.viewModel.nYearProperty());

		this.sigLevelTextField.textProperty().bindBidirectional(this.viewModel.sigLevelProperty());

		this.errorMessageLabel.textProperty().bindBidirectional(this.viewModel.errorMessageProperty());

	}

	private void setupListenerForErrorMessage() {
		this.eventDate.setOnAction(e -> this.errorMessageLabel.setText(""));

		this.descriptionTextField.textProperty()
				.addListener((observable, oldValue, newValue) -> this.errorMessageLabel.setText(""));
	}

	// Part 2: uncomment the commented part below
	private void setupListenerForListView() {
		this.eventListView.getSelectionModel().selectedItemProperty().addListener((observable, oldEvent, newEvent) -> {
			if (newEvent != null) {

				this.descriptionTextField.textProperty().set(newEvent.getDescription());
				this.eventDate.valueProperty().set(newEvent.getDate());

				this.clearNyeartextfield();
				this.clearSigLeveltextfield();
				if (newEvent instanceof Daily) {
					this.dailyRadioButton.setSelected(true);
				} else if (newEvent instanceof Monthly) {
					this.monthlyRadiobutton.setSelected(true);
				} else if (newEvent instanceof Onetime) {
					this.onetimeRadioButton.setSelected(true);
					int sigLevel = ((Onetime) newEvent).getSigLevel();
					this.sigLevelTextField.textProperty().set(Integer.toString(sigLevel));
				} else if (newEvent instanceof NYear) {
					this.nyearRadioButton.setSelected(true);
					int nyear = ((NYear) newEvent).getN();
					this.nyearTextField.textProperty().set(Integer.toString(nyear));
				} else {
					this.errorMessageLabel.setText("invalid event");
				}

			}
		});
	}

	// Part 3
	@FXML
	void onEarliestEventByTypeClick(ActionEvent event) {
		this.errorMessageLabel.setText("");
		this.viewModel.getEarliestEventByType();
		this.displayEvents();
	}

	// Part 3
	@FXML
	void onLoadRandomClick(ActionEvent event) {
		this.errorMessageLabel.setText("");
		this.viewModel.loadRandom();
		this.displayEvents();
	}

	@FXML
	void onSortbyDateClick(ActionEvent event) {
		this.errorMessageLabel.setText("");
		this.viewModel.sortByDate();
		this.displayEvents();
	}

	@FXML
	void onSortbyTypeDateClick(ActionEvent event) {
		this.errorMessageLabel.setText("");
		this.viewModel.sortByTypeDate();
		this.displayEvents();
	}

	@FXML
	void onNYearRadioClick(ActionEvent event) {
		this.nyearTextField.setDisable(false);
		this.clearSigLeveltextfield();
	}

	@FXML
	void onMonthlyRadioClick(ActionEvent event) {
		this.clearNyeartextfield();
		this.clearSigLeveltextfield();
	}

	@FXML
	void onDailyRadioClick(ActionEvent event) {
		this.clearNyeartextfield();
		this.clearSigLeveltextfield();
	}

	@FXML
	void onOnetimeRadioClick(ActionEvent event) {
		this.sigLevelTextField.setDisable(false);
		this.clearNyeartextfield();
	}

	private void clearNyeartextfield() {
		this.nyearTextField.setText("");
		this.nyearTextField.setDisable(true);
	}

	private void clearSigLeveltextfield() {
		this.sigLevelTextField.setText("");
		this.sigLevelTextField.setDisable(true);
	}

	@FXML
	void onAddButtonClick(ActionEvent event) {
		RadioButton selected = (RadioButton) this.group.getSelectedToggle();
		String type = selected.getText();
		if (this.viewModel.addEvent(type)) {
			this.clear();
			this.displayEvents();
		}

	}

	@FXML
	void onClearButtonClick(ActionEvent event) {
		this.clear();

	}

	@FXML
	void onClearListButtonClick(ActionEvent event) {
		this.clearList();

	}

	private void clearList() {
		this.viewModel.clearCalendarBookandListView();

	}

	@FXML
	void onSearchButtonClick(ActionEvent event) {

		this.viewModel.search();
		this.displayEvents();

	}

	@FXML
	void onReloadButtonClick(ActionEvent event) {
		this.clear();
		this.viewModel.reloadEvents();

	}

	// Part 1: Uncomment the code below for Part 1 Save feature
	@FXML
	void onSaveClick(ActionEvent event) {
		if (this.viewModel.getCalendarBook().size() == 0) {
			this.showErrorDialog(UI.ExceptionMessages.SAVE_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.NO_EVENT_WHILE_SAVING);
			return;
		}

		FileChooser chooser = this.initializeFileChooser("Output file");

		File outputFile = chooser.showSaveDialog(null);
		if (outputFile == null) {
			return;
		}
		try {

			this.viewModel.saveFile(outputFile);
		} catch (IOException saveException) {
			this.showErrorDialog(UI.ExceptionMessages.SAVE_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.SAVE_DATA_ERROR_MESSAGE);
		} catch (Exception ex) {
			this.showErrorDialog("Error!", ex.getMessage());

		}
	}

	// Part 2: Uncomment the code below for Part 2 Save feature
	@FXML
	void onSave2Click(ActionEvent event) {

		if (this.viewModel.getCalendarBook().size() == 0) {
			this.showErrorDialog(UI.ExceptionMessages.SAVE_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.NO_EVENT_WHILE_SAVING);
			return;
		}

		FileChooser chooser = this.initializeFileChooser("Output file");

		File outputFile = chooser.showSaveDialog(null);
		if (outputFile == null) {
			return;
		}
		try {

			this.viewModel.saveFile2(outputFile);
		} catch (IOException saveException) {
			this.showErrorDialog(UI.ExceptionMessages.SAVE_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.SAVE_DATA_ERROR_MESSAGE);
		} catch (Exception ex) {
			this.showErrorDialog("Error!", ex.getMessage());

		}

	}

	// Part 3: Uncomment the code below for Part 3 Save feature

	@FXML
	void onSave3Click(ActionEvent event) {

		if (this.viewModel.getCalendarBook().size() == 0) {
			this.showErrorDialog(UI.ExceptionMessages.SAVE_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.NO_EVENT_WHILE_SAVING);
			return;
		}

		FileChooser chooser = this.initializeFileChooser("Output file");

		File outputFile = chooser.showSaveDialog(null);
		if (outputFile == null) {
			return;
		}
		
		try {
			
			this.viewModel.saveFile3(outputFile);
		} catch (IOException saveException) {
			this.showErrorDialog(UI.ExceptionMessages.SAVE_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.SAVE_DATA_ERROR_MESSAGE);
		} catch (Exception ex) {
			this.showErrorDialog("Error!", ex.getMessage());

		}
	}

	@FXML
	void onLoadWebClick(ActionEvent event) {
		this.clear();
		this.handleUrlLoad();
	}

	@FXML
	void onLoadFileClick(ActionEvent event) {
		this.clear();
		this.handleFileLoad();
	}

	@FXML
	void onExitCick(ActionEvent event) {
		Platform.exit();
	}

	private void showErrorDialog(String title, String message) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText("Error!");
		alert.setContentText(message);

		alert.showAndWait();

	}

	private void displayEvents() {
		if (this.viewModel.eventListProperty() != null && this.viewModel.eventListProperty().size() != 0) {
			this.eventListView.setItems(this.viewModel.eventListProperty());
		}
	}

	private void handleUrlLoad() {

		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle(UI.ExceptionMessages.TEXTINPUT_TITLE);
		dialog.setContentText(UI.ExceptionMessages.ENTER_URL);

		Optional<String> result = dialog.showAndWait();

		result.ifPresent(urlString -> this.loadUrl(urlString));

	}

	private void loadUrl(String urlString) {
		try {
			this.viewModel.loadUrl(new URL(urlString));

			this.displayEvents();

		} catch (MalformedURLException urlException) {
			this.showErrorDialog(UI.ExceptionMessages.LOAD_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.INVALID_URL_MESSAGE);

		} catch (IOException readException) {
			this.showErrorDialog(UI.ExceptionMessages.LOAD_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.READ_DATA_ERROR_MESSAGE);

		}
	}

	private void handleFileLoad() {
		FileChooser chooser = this.initializeFileChooser("Input file");

		File inputFile = chooser.showOpenDialog(null);
		if (inputFile == null) {
			return;
		}
		try {
			this.viewModel.loadFile(inputFile);
			this.displayEvents();
		} catch (IOException readException) {
			this.showErrorDialog(UI.ExceptionMessages.LOAD_DIALOG_ERROR_TITLE,
					UI.ExceptionMessages.READ_DATA_ERROR_MESSAGE);
		} catch (Exception ex) {
			this.showErrorDialog("Error", ex.getMessage());
		}

	}

	private FileChooser initializeFileChooser(String title) {
		FileChooser chooser = new FileChooser();
		chooser.getExtensionFilters().add(new ExtensionFilter("txt Files", "*.txt"));
		chooser.setTitle(title);
		return chooser;
	}

	private void clear() {
		this.descriptionTextField.setText("");
		this.eventDate.setValue(null);
		this.sigLevelTextField.setText("");
		this.nyearTextField.setText("");
		this.errorMessageLabel.setText("");
		this.dailyRadioButton.setSelected(true);
		this.sigLevelTextField.setDisable(true);
		this.nyearTextField.setDisable(true);
	}

}
